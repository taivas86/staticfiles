Storage directory
``
filesdirectory
``

Basic auth setup
```
volumes:
 - ./.docker/nginx/conf.d/.htpasswd:/etc/nginx/conf.d/.htpasswd
``` 

Test creds
```
login: test
pass: test
```

Generate new user
```
sudo apt install apache2-utils 
sudo htpasswd .docker/nginx/conf.d.htpasswd user2
```
more info 
``https://docs.nginx.com/nginx/admin-guide/security-controls/configuring-http-basic-authentication/
``

Test URLs
```
http://localhost:8081/sample1.pdf
http://localhost:8081/sample2.pdf
```
Test /indexed/ URL
```
http://localhost:8081/indexed/
http://localhost:8081/indexed/index1.pdf
http://localhost:8081/indexed/index2.pdf
```
"Basic auth URLS"
```
http://localhost:8081/basicauthenticated/
http://localhost:8081/basicauthenticated/porst-full.jpg
http://localhost:8081/basicauthenticated/post-prev.jpg
http://localhost:8081/basicauthenticated/update-prev.jpg
```